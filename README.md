# CityVR: Gameful Software Visualization #
Gamification of software engineering tasks improve developer engagement, but has been limited to mechanisms such as points and badges. We believe that a tool that provides developers an interface analogous to computer games can represent the gamification of software engineering tasks more effectively via software visualization. We introduce CityVR � an interactive software visualization tool that implements the city metaphor technique using virtual reality in an immersive 3D environment medium to boost developer engagement in software comprehension tasks. We evaluated our tool with a case study based on ArgoUML. We measured engagement in terms of feelings, interaction, and time perception. We report on how our design choices relate to developer engagement. We found that developers i) felt curious, immersed, in control, excited, and challenged, ii) spent considerable interaction time navigating and selecting elements, and iii) perceived that time passed faster than in reality, and therefore were willing to spend more time using the tool to solve software engineering tasks.

### What is this repository for? ###
[Watch a video](https://youtu.be/R0C-HMAtgnk)

### How do I get set up? ###
* You need an HTC Vive device
* Check the project source code out
* Open the project with Unity (Unity will download the required libraries if needed)
* Download [ArgoUML source code](http://scg.unibe.ch/research/cityvr/ArgoUML-source-code?view=PRDownloadView) (to run the demo)
* Run it

### Who do I talk to? ###
For comments or questions please visit the [tool's website](http://scg.unibe.ch/research/cityvr) or contact me at leonelmerino@gmail.com